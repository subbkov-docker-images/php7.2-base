FROM ubuntu:16.04

MAINTAINER Subbkov <https://gitlab.com/Subbkov>

COPY ondrey-ubuntu-php-xenial.list /etc/apt/sources.list.d/

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E5267A6C > /dev/null

RUN apt-get -qq update \
    && apt-get install -y --no-install-recommends git ca-certificates ghostscript gsfonts make php-pear \
    php7.2-dev php7.2-curl php7.2-gd php7.2-imap php7.2-intl php7.2-mbstring php7.2-mysql php7.2-soap \
    php7.2-sqlite3 php7.2-xml php7.2-zip php7.2-bcmath \
    php-apcu php-imagick php-memcache php-redis php-ssh2 php-igbinary libxrender1 > /dev/null \
# libxrender1 seems to be needed by certain php based pdf generators
# xdebug needs to be installed manually as it is still in alpha
    && pear config-set preferred_state alpha \
    && pecl install xdebug > /dev/null \
# cleanup and replace dev php with regular
    && apt-get -y install --no-install-recommends make- php-pear- php7.2-dev- php7.2-cli > /dev/null \
    && apt-get -y autoremove > /dev/null \
    && apt-get -y clean > /dev/null \
    && rm -rf /var/lib/apt/lists/* > /dev/null

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/local/bin/composer \
    && composer clear-cache

COPY conf.d/ /etc/php/7.2/mods-available/